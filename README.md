Log analysis for AD and ELENA
===

The following scripts are examples/production scripts to make simple queries to NXCALS (via pyTimber) to measure the stability of AD/ELENA decelerators.

You can easily clone the repository directly in your SWAN: e.g. [ADE_ELENA_NXCALS_Analysis](https://cern.ch/swanserver/cgi-bin/go?projurl=https://gitlab.cern.ch/ELENA/nxcals-analysis.git)
Note that in this case SWAN will make your private copy of this python notebook, i.e. it will not be available to others.

The best way to contribute, is to `clone` this `git` repository in your CERNbox/EOS folder, so that you can edit it and eventually commit your changes to the repository!

> WARNING!
> 1. When starting your **SWAN** session, remember to select "NXCals Spark 3" as "Software stack".
>
> 2. In order to access NXCALS data, you need to be allowed to do so - see [NXCALS Access Request](http://nxcals-docs.web.cern.ch/current/user-guide/data-access/nxcals-access-request/)
>
> 3. You will need to enter your CERN password when calling `ldb = pytimber.LoggingDB(source="nxcals")`. This will only work if step 2. above has been fully completed...



